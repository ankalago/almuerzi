const express = require('express')
const Orders = require('../models/Orders')
const {isAuthenticated, hasRole} = require('../auth')

const router = express.Router()

router.get('/', (req, res) => {
    Orders.find()
        .exec()
        .then(x => res.status(200).send(x))
    // res.send('GET orders')
})

router.get('/:id', (req, res) => {
    Orders.findById(req.params.id)
        .exec()
        .then(x => res.status(200).send(x))
    // res.send('GET orders')
})

router.post('/', isAuthenticated, (req, res) => {
    const {_id} = req.user
    Orders.create({...req.body, user_id: _id})
        .then(x => res.status(201).send(x))
    // res.send('POST orders')
})

// router.put('/:id', isAuthenticated, hasRole('user'), (req, res) => { // TODO validate rol
// router.put('/:id', isAuthenticated, hasRoles(['user', 'admin']), (req, res) => { // TODO validate rol array
router.put('/:id', isAuthenticated, (req, res) => {
    Orders.findOneAndUpdate(req.params.id, req.body)
        .then(x => res.sendStatus(204))
    // res.send('PUT orders')
})

router.delete('/:id', isAuthenticated, (req, res) => {
    Orders.findByIdAndDelete(req.params.id)
        .then(x => res.sendStatus(204))
    // res.send('DELETE orders')
})

module.exports = router;
