const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')

const meals = require('./routes/meals')
const orders = require('./routes/orders')
const auth = require('./routes/auth')

const app = express()
app.use(bodyParser.json()) // add plugins body parse
app.use(cors()) // add plugin cors

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    serverSelectionTimeoutMS: 10000, // Keep trying to send operations for 5 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
};
mongoose.connect(process.env.MONGO_URI, options)

app.use("/api/meals", meals)
app.use("/api/orders", orders)
app.use("/api/auth", auth)

module.exports = app
